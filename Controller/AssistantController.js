(function () {

    // 取得設定資料
    const configs = require('../config.js');

    // 引用寫好的API
    const assistantAPI = require('../services/AssistantAPIV2');
    assistantAPI.init(configs.assistant)

    // 存放 sdk 產生之 session
    let userSessions = {};

    class AssistantController {
        constructor() {
            this.sendMessage = this.sendMessage.bind(this);
        }

        // 取得 user session 
        async sendMessage(userId, text) {

            // 取出API所需暫存Session
            let userSession = userSessions[userId];

            // 若沒有對應session
            if (!userSession) {
                // create new session
                let newSession = await assistantAPI.createSession();
                
                userSession = newSession.session_id;
                userSessions[userId] = userSession;
            }

            // QA
            let assistantAns = await assistantAPI.message(text, userSessions[userId]);
            console.log(JSON.stringify(assistantAns, null, 2));

            let responseContent = assistantAns.output.generic[0].text;
            console.log('responseContent : ' , responseContent);

            return {
                type: 'text',
                text: responseContent
            }
        }
    }
    module.exports = new AssistantController();
}());
var express = require('express');
var router = express.Router();
const configs = require('../config.js');
const ChannelAccess = configs.channelAccessToken;
const line = require('@line/bot-sdk');
const MongodbCtl = require('../Controller/MongodbController');
const client = new line.Client({
    channelAccessToken: ChannelAccess
});
const AssistantCtl = require('../Controller/AssistantController');
const Logger = require('../Controller/Logger');

Logger.init('webhook');
MongodbCtl.init();

router.post('/line', async function (req, res, next) {
    // 印出 webhook 傳入內容
    Logger.log('webhook received an event');
    res.status(200).send();

    let targetEvent = req.body.events[0];
    if (targetEvent.type == 'message') {
        if (targetEvent.message.type == 'text') {
            let userSay = targetEvent.message.text;
            let userId = targetEvent.source.userId;

            Logger.log('userID:', userId);
            Logger.log('userSay:', userSay);

            let newMessage = await AssistantCtl.sendMessage(userId, userSay);
            console.log('newMessage: ', newMessage);

            let document = {
                userId: userId,
                userSay: userSay,
                returnMessage: newMessage,
                timestamp: Date.now()
            }
            MongodbCtl.insertOne('LineData', 'chatLog', document);
            replyToLine(targetEvent.replyToken, newMessage);
            Logger.log(userId, "return message: ", newMessage);   
        }
    }
});

module.exports = router;

function replyToLine(rplyToken, message) {

    client.replyMessage(rplyToken, message)
        .then((res) => {
            console.log(res)
            return true;
        })
        .catch((err) => {
            console.log(err)
            return false;
        });
}
